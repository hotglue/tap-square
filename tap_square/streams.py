"""Stream type classes for tap-square."""
import requests
from email.policy import default
from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable
from xml.dom.minicompat import StringTypes
import copy
from singer_sdk.helpers._state import (
    finalize_state_progress_markers,
    log_sort_error
)
from singer_sdk.exceptions import InvalidStreamSortException
import math

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_square.client import SquareStream
import uuid


class UsersStream(SquareStream):
    """Define custom stream."""
    name = "users"
    path = "/users"
    primary_keys = ["id"]
    replication_key = None
    # Optionally, you may also use `schema_filepath` in place of `schema`:
    # schema_filepath = SCHEMAS_DIR / "users.json"
    schema = th.PropertiesList(
        th.Property("name", th.StringType),
        th.Property(
            "id",
            th.StringType,
            description="The user's system ID"
        ),
        th.Property(
            "age",
            th.IntegerType,
            description="The user's age in years"
        ),
        th.Property(
            "email",
            th.StringType,
            description="The user's email address"
        ),
        th.Property("street", th.StringType),
        th.Property("city", th.StringType),
        th.Property(
            "state",
            th.StringType,
            description="State name in ISO 3166-2 format"
        ),
        th.Property("zip", th.StringType),
    ).to_dict()


class GroupsStream(SquareStream):
    """Define custom stream."""
    name = "groups"
    path = "/groups"
    primary_keys = ["id"]
    replication_key = "modified"
    schema = th.PropertiesList(
        th.Property("name", th.StringType),
        th.Property("id", th.StringType),
        th.Property("modified", th.DateTimeType),
    ).to_dict()

class CatalogStream(SquareStream):

    name = "catalogs"
    path = "/catalog/list"
    primary_keys = ["id"]
    
    schema = th.PropertiesList(
        th.Property("type", th.StringType),
        th.Property("id", th.StringType),
        th.Property("updated_at", th.DateTimeType),
        th.Property("created_at", th.DateTimeType),
        th.Property("version", th.NumberType),
        th.Property("is_deleted", th.BooleanType),
        th.Property("custom_attribute_values", th.CustomType({"type": ["object", "string","null"]}), required=True, default=""), # this key value will change, so taking generic
        th.Property("present_at_all_locations", th.BooleanType, required=True, default=""),
        th.Property("present_at_location_ids", th.ArrayType(th.StringType), required=True, default=""),
        th.Property("category_data", th.ObjectType(
                th.Property("name", th.StringType)
        )),
        th.Property("item_data", th.ObjectType(
            th.Property("name", th.StringType),
            th.Property("description", th.StringType),
            th.Property("visibility", th.StringType ),
            th.Property("vategory_id", th.StringType ),
            th.Property("variations", th.ArrayType(
                th.ObjectType(
                    th.Property("type", th.StringType),
                    th.Property("id", th.StringType),
                    th.Property("updated_at", th.DateTimeType),
                    th.Property("created_at", th.DateTimeType),
                    th.Property("version", th.NumberType),
                    th.Property("is_deleted", th.BooleanType),
                    th.Property("present_at_all_locations", th.BooleanType),
                    th.Property("present_at_location_ids", th.ArrayType(th.StringType)),
                    th.Property("item_variation_data", th.ObjectType(
                        th.Property("item_id", th.StringType),
                        th.Property("name", th.StringType),
                        th.Property("sku", th.StringType),
                        th.Property("ordinal", th.IntegerType),
                        th.Property("pricing_type", th.StringType),
                        th.Property("price_money", th.ObjectType(
                            th.Property("amount", th.NumberType),
                            th.Property("currency", th.StringType),
                        )),
                        th.Property("location_overrides", th.ArrayType(
                            th.ObjectType(
                                th.Property("location_id", th.StringType),
                                th.Property("track_inventory", th.BooleanType),
                            ),
                        )),
                        th.Property("sellable", th.BooleanType),
                        th.Property("stockable", th.BooleanType),
                    )),
                )
            )),
            th.Property("product_type", th.StringType),
            th.Property("skip_modifier_screen", th.BooleanType,),
            th.Property("ecom_visibility", th.StringType),
            th.Property("image_ids", th.ArrayType(th.StringType))

        ))
    ).to_dict()
    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        variations = []
        if record.get("item_data"):
            for variation in record['item_data'].get("variations"):
                variations.append({"id":variation['id']})
        if variations:        
            return {"item_data":variations}
        else:
            return None
        
class LocationsStream(SquareStream):
    """Define Locations stream."""

    name = "locations"
    path = "/locations"
    primary_keys = ["id"]
    replication_key = None
    records_jsonpath = "$.locations[*]" 

    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("name", th.StringType),
        th.Property("address", th.ObjectType(
            th.Property("address_line_1", th.StringType),
            th.Property("locality", th.StringType),
            th.Property("administrative_district_level_1", th.DateTimeType),
            th.Property("postal_code", th.StringType),
            th.Property("country", th.StringType),
        )),
        th.Property("timezone", th.StringType),
        th.Property("capabilities", th.ArrayType(th.StringType)),
        th.Property("status", th.StringType),
        th.Property("created_at", th.DateTimeType),
        th.Property("merchant_id", th.StringType),
        th.Property("country", th.StringType),
        th.Property("language_code", th.StringType),
        th.Property("currency", th.StringType),
        th.Property("phone_number", th.StringType),
        th.Property("business_name", th.StringType),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return  record["id"]
    
    def _sync_records( 
        self, context: Optional[dict] = None
    ) -> None:
        record_count = 0
        current_context: Optional[dict]
        context_list: Optional[List[dict]]
        context_list = [context] if context is not None else self.partitions
        selected = self.selected
        limit = 10


        for current_context in context_list or [{}]:
            partition_record_count = 0
            current_context = current_context or None
            state = self.get_context_state(current_context)
            state_partition_context = self._get_state_partition_context(current_context)
            self._write_starting_replication_value(current_context)
            child_context: Optional[dict] = (
                None if current_context is None else copy.copy(current_context)
            )

            total_records_count = len([ 1 for record in self.get_records(current_context)])
            sync_count = 1
            self.location_ids = []
            max_num_syncs = math.ceil(total_records_count/limit)
            for record_result in self.get_records(current_context):
                if isinstance(record_result, tuple):
                    # Tuple items should be the record and the child context
                    record, child_context = record_result
                else:
                    record = record_result
                child_context = copy.copy(
                    self.get_child_context(record=record, context=child_context)
                )
                self.location_ids.append(child_context)
                for key, val in (state_partition_context or {}).items():
                    # Add state context to records if not already present
                    if key not in record:
                        record[key] = val

                # Sync children, except when primary mapper filters out the record
                if self.stream_maps[0].get_filter_result(record):
                    if len(self.location_ids)==limit or (sync_count == max_num_syncs and len(self.location_ids) == total_records_count%limit):
                        sync_count = sync_count + 1
                        new_context = {"location_ids": self.location_ids}
                        self._sync_children(new_context)
                        self.location_ids = []
                    
                self._check_max_record_limit(record_count)
                if selected:
                    if (record_count - 1) % self.STATE_MSG_FREQUENCY == 0:
                        self._write_state_message()
                    self._write_record_message(record)
                    try:
                        self._increment_stream_state(record, context=current_context)
                    except InvalidStreamSortException as ex:
                        log_sort_error(
                            log_fn=self.logger.error,
                            ex=ex,
                            record_count=record_count + 1,
                            partition_record_count=partition_record_count + 1,
                            current_context=current_context,
                            state_partition_context=state_partition_context,
                            stream_name=self.name,
                        )
                        raise ex

                record_count += 1
                partition_record_count += 1
            if current_context == state_partition_context:
                # Finalize per-partition state only if 1:1 with context
                finalize_state_progress_markers(state)
        if not context:
            # Finalize total stream only if we have the full full context.
            # Otherwise will be finalized by tap at end of sync.
            finalize_state_progress_markers(self.stream_state)
        self._write_record_count_log(record_count=record_count, context=context)
        # Reset interim bookmarks before emitting final STATE message:
        self._write_state_message()

class OrdersStream(SquareStream):
    name = "orders"
    path = "/orders/search?types=items"
    primary_keys = ["id"]
    rest_method = "POST"
    records_jsonpath = "$.orders[*]"
    parent_stream_type = LocationsStream

    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("location_id", th.StringType),
        th.Property("line_items", th.ArrayType(
            th.ObjectType(
                th.Property("uid", th.StringType),
                th.Property("catalog_object_id", th.StringType),
                th.Property("catalog_version", th.NumberType),
                th.Property("quantity", th.StringType),
                th.Property("name", th.StringType),
                th.Property("variation_name", th.StringType),
                th.Property("base_price_money", th.ObjectType(
                    th.Property("amount", th.IntegerType),
                    th.Property("currency", th.StringType)
                )),
                th.Property("gross_sales_money", th.ObjectType(
                    th.Property("amount", th.IntegerType),
                    th.Property("currency", th.StringType)
                )),
                th.Property("total_tax_money", th.ObjectType(
                    th.Property("amount", th.IntegerType),
                    th.Property("currency", th.StringType)
                )),
                th.Property("total_discount_money", th.ObjectType(
                    th.Property("amount", th.IntegerType),
                    th.Property("currency", th.StringType)
                )),
                th.Property("total_money", th.ObjectType(
                    th.Property("amount", th.IntegerType),
                    th.Property("currency", th.StringType)
                )),
                th.Property("variation_total_price_money", th.ObjectType(
                    th.Property("amount", th.IntegerType),
                    th.Property("currency", th.StringType)
                )),
                th.Property("item_type", th.StringType)
                
            )
        )),
        th.Property("created_at", th.DateTimeType),
        th.Property("updated_at", th.DateTimeType),
        th.Property("state", th.StringType),
        th.Property("version", th.IntegerType),
        th.Property("total_tax_money", th.ObjectType(
            th.Property("amount", th.IntegerType),
            th.Property("currency", th.StringType)
        )),
        th.Property("total_discount_money", th.ObjectType(
            th.Property("amount", th.IntegerType),
            th.Property("currency", th.StringType)
        )),
        th.Property("total_tip_money", th.ObjectType(
            th.Property("amount", th.IntegerType),
            th.Property("currency", th.StringType)
        )),
        th.Property("total_money", th.ObjectType(
            th.Property("amount", th.IntegerType),
            th.Property("currency", th.StringType)
        )),
        th.Property("closed_at", th.StringType),
        th.Property("tenders", th.ArrayType(
            th.ObjectType(
                th.Property("id", th.StringType),
                th.Property("location_id", th.StringType),
                th.Property("transaction_id", th.StringType),
                th.Property("created_at", th.StringType),
                th.Property("amount_money", th.ObjectType(
                    th.Property("amount", th.IntegerType),
                    th.Property("currency", th.StringType)
                )),
                th.Property("type", th.StringType),
                th.Property("card_details", th.ObjectType(
                    th.Property("status", th.StringType),
                    th.Property("card", th.ObjectType(
                        th.Property("card_brand", th.StringType),
                        th.Property("last_4", th.StringType),
                        th.Property("fingerprint", th.StringType),
                    )),
                    th.Property("entry_method", th.StringType),
                )),
                th.Property("payment_id", th.StringType),
                
            )),

        ),
        th.Property("total_service_charge_money", th.ObjectType(
            th.Property("amount", th.IntegerType),
            th.Property("currency", th.StringType),
        )),
        th.Property("net_amounts", th.ObjectType(
            th.Property("total_money", th.ObjectType(
                th.Property("amount", th.IntegerType),
                th.Property("currency", th.StringType)
            )),
            th.Property("tax_money", th.ObjectType(
                th.Property("amount", th.IntegerType),
                th.Property("currency", th.StringType)
            )),
            th.Property("discount_money", th.ObjectType(
                th.Property("amount", th.IntegerType),
                th.Property("currency", th.StringType)
            )),
            th.Property("trip_money", th.ObjectType(
                th.Property("amount", th.IntegerType),
                th.Property("currency", th.StringType)
            )),
            th.Property("service_charge_money", th.ObjectType(
                th.Property("amount", th.IntegerType),
                th.Property("currency", th.StringType)
            )),

        )),
        th.Property("source", th.ObjectType(
            th.Property("name", th.StringType)
        )),
        th.Property("customer_id", th.StringType),


    ).to_dict()

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        return response.json().get("cursor")

    def prepare_request_payload(
            self, context: Optional[dict], next_page_token: Optional[Any]
        ) -> Optional[dict]:
            body = {
                "location_ids": context["location_ids"],
                "limit" : 500,
                "cursor" : next_page_token
            }
            return body

class CatalogItemVariationsStream(SquareStream):

    name = "catalog_item_variations"
    path = None
    primary_keys = ["id"]
    parent_stream_type = CatalogStream
    prevous_items = []
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
    ).to_dict()
    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return  {"catalog_object_id":record["id"]}
    def get_records(self, context: Optional[dict]) -> Iterable[Dict[str, Any]]:
        records = []
        if context and context.get("item_data"):
            records = context['item_data']
        for record in records:
            transformed_record = self.post_process(record, context)
            if transformed_record['id'] in self.prevous_items:
                return None
            if transformed_record is None:
                # Record filtered out during post_process()
                continue
            self.prevous_items.append(transformed_record['id'])
            yield transformed_record.copy()
class CatalogItemVariationCountsStream(SquareStream):

    name = "catalog_item_variation_counts"
    path = "/inventory/{catalog_object_id}"
    primary_keys = ["id"]
    parent_stream_type = CatalogItemVariationsStream
    records_jsonpath = "$.counts[*]"
    
    schema = th.PropertiesList(
        th.Property("catalog_object_id", th.StringType),
        th.Property("id", th.StringType),
        th.Property("catalog_object_type", th.StringType),
        th.Property("state", th.DateTimeType),
        th.Property("location_id", th.DateTimeType),
        th.Property("quantity", th.StringType),
        th.Property("calculated_at", th.DateTimeType),
    ).to_dict()

class VendorsStream(SquareStream):
    """Define custom stream."""
    name = "vendors"
    path = "/vendors/search"
    primary_keys = ["id"]
    rest_method = "POST"
    records_jsonpath = "$.vendors[*]"
    replication_key = None
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("name", th.StringType),
        th.Property("created_at", th.DateTimeType),
        th.Property("updated_at", th.DateTimeType),
        th.Property("address", th.CustomType({"type": ["object", "string"]})),
        th.Property("version", th.NumberType),
        th.Property("status", th.StringType),
    ).to_dict()
    def prepare_request_payload(
            self, context: Optional[dict], next_page_token: Optional[Any]
        ) -> Optional[dict]:
            body = {
                "filter":{
                     "status": [
                        "ACTIVE", "INACTIVE"
                    ],
                },
                "sort" : {
                    "field":"CREATED_AT",
                    "order":"ASC"
                },
                "cursor" : next_page_token
            }
            return body
class RefundsStream(SquareStream):
    name = "refunds"
    path = "/refunds"
    primary_keys = ["id"]
    records_jsonpath = "$.refunds[*]"
    replication_key = "updated_at"
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("status", th.StringType),
        th.Property("amount_money", th.CustomType({"type": ["object", "string"]})),
        th.Property("payment_id", th.StringType),
        th.Property("order_id", th.StringType),
        th.Property("order_id", th.StringType),
        th.Property("created_at", th.DateTimeType),
        th.Property("updated_at", th.DateTimeType),
        th.Property("location_id", th.CustomType({"type": ["object", "string"]})),
        th.Property("destination_type", th.StringType),
    ).to_dict()
class PayoutsStream(SquareStream):
    name = "payouts"
    path = "/payouts"
    primary_keys = ["id"]
    records_jsonpath = "$.payouts[*]"
    replication_key = "updated_at"
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("status", th.StringType),
        th.Property("location_id", th.StringType),
        th.Property("created_at", th.DateTimeType),
        th.Property("updated_at", th.DateTimeType),
        th.Property("amount_money", th.CustomType({"type": ["object", "string"]})),
        th.Property("destination", th.CustomType({"type": ["object", "string"]})),
        th.Property("version", th.NumberType),
        th.Property("type", th.StringType),
        th.Property("arrival_date", th.StringType),
        th.Property("end_to_end_id", th.StringType),
    ).to_dict()
class BatchInventoryCountsStream(SquareStream):
    name = "batch_inventory_counts"
    path = "/inventory/counts/batch-retrieve"
    rest_method = "POST"
    primary_keys = None
    records_jsonpath = "$.counts[*]"
    replication_key = None
    schema = th.PropertiesList(
        th.Property("catalog_object_id", th.StringType),
        th.Property("catalog_object_type", th.StringType),
        th.Property("state", th.StringType),
        th.Property("location_id", th.StringType),
        th.Property("quantity", th.CustomType({"type": ["number", "string"]})),
        th.Property("calculated_at", th.DateTimeType),
    ).to_dict()

    def prepare_request_payload(
            self, context: Optional[dict], next_page_token: Optional[Any]
        ) -> Optional[dict]:
            body = {
                #Enable if location based inventory counts are required
                # "location_ids": context["location_ids"],
                "limit" : 500,
                "cursor" : next_page_token
            }
            return body
    
