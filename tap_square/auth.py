import json
from datetime import datetime
from typing import Any, Dict, Optional

import requests
from singer_sdk.authenticators import APIAuthenticatorBase
from singer_sdk.streams import Stream as RESTStreamBase


class OAuth2Authenticator(APIAuthenticatorBase):
    def __init__(
        self,
        stream: RESTStreamBase,
        config_file: Optional[str] = None,
        auth_endpoint: Optional[str] = None,
    ) -> None:
        super().__init__(stream=stream)
        self._auth_endpoint = auth_endpoint
        self._config_file = config_file
        self._tap = stream._tap

    @property
    def auth_headers(self) -> dict:
        if not self.is_token_valid():
            self.update_access_token()
        result = super().auth_headers
        result["Authorization"] = f"Bearer {self._tap._config.get('access_token')}"
        return result

    @property
    def oauth_request_body(self) -> dict:
        """Define the OAuth request body for the hubspot API."""
        return {
            "refresh_token": self._tap._config["refresh_token"],
            "grant_type": "refresh_token",
            "client_id": self._tap._config["client_id"],
            "client_secret": self._tap._config["client_secret"],
        }

    def is_token_valid(self) -> bool:
        access_token = self._tap._config.get("access_token")
        expires_in = self._tap._config.get("expires_at",0)

        if expires_in:
            expires_in = self.calculate_expires_in(expires_in)

        if access_token and not expires_in:
            return False

        return not bool(
            (not access_token) or (not expires_in) or ((expires_in) < 120)
        )

    
    def calculate_expires_in(self,expires_at):
        given_datetime = datetime.strptime(expires_at, "%Y-%m-%dT%H:%M:%SZ")
        current_datetime = datetime.utcnow()

        time_difference = given_datetime - current_datetime
        seconds_left = int(time_difference.total_seconds())
        return seconds_left


    def update_access_token(self) -> None:
        headers = {"Content-Type": "application/x-www-form-urlencoded"}
        token_response = requests.post(
            self._auth_endpoint, data=self.oauth_request_body, headers=headers
        )

        if (
            token_response.json().get("error_description")
            == "Rate limit exceeded: access_token not expired"
        ):
            return None

        try:
            token_response.raise_for_status()
            self.logger.info("OAuth authorization attempt was successful.")
        except Exception as ex:
            raise RuntimeError(
                f"Failed OAuth login, response was '{token_response.json()}'. {ex}"
            )
        token_json = token_response.json()
        self.access_token = token_json["access_token"]

        if not token_json.get("expires_at"):
            self.logger.debug(
                "No expires_in receied in OAuth response and no "
                "default_expiration set. Token will be treated as if it never "
                "expires.",
            )


        self._tap._config["access_token"] = token_json["access_token"]
        self._tap._config["refresh_token"] = token_json["refresh_token"]
        self._tap._config["expires_at"] = token_json.get("expires_at")

        with open(self._tap.config_file, "w") as outfile:
            json.dump(self._tap._config, outfile, indent=4)
