"""REST client handling, including SquareStream base class."""

import requests
from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable , Callable
import backoff
from memoization import cached
from singer_sdk.exceptions import FatalAPIError, RetriableAPIError
from singer_sdk.helpers.jsonpath import extract_jsonpath
from singer_sdk.streams import RESTStream
from tap_square.auth import OAuth2Authenticator
from pendulum import parse

SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")


class SquareStream(RESTStream):
    """Square stream class."""

    @property
    def url_base(self) -> str:
        if self.config.get('is_sandbox'):
            return 'https://connect.squareupsandbox.com/v2'
        else: 
            return "https://connect.squareup.com/v2"

    records_jsonpath = "$.objects[*]"  # Or override `parse_response`.
    next_page_token_jsonpath = "$.cursor"  # Or override `get_next_page_token`.

    @property
    def authenticator(self) -> OAuth2Authenticator:
        base_endpoint = self.url_base.rstrip("/v2")
        return OAuth2Authenticator(self, self.config, f"{base_endpoint}/oauth2/token")


    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        #vendors stream needs specific version of API
        if self.name=="vendors":
            headers['Square-Version'] = "2023-05-17"
        return headers

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Any:

        if self.next_page_token_jsonpath:
            all_matches = extract_jsonpath(
                self.next_page_token_jsonpath, response.json()
            )
            first_match = next(iter(all_matches), None)
            if first_match == previous_token:
                return None
            next_page_token = first_match
        else:
            next_page_token = response.headers.get("X-Next-Page", None)

        return next_page_token

    def get_starting_time(self, context):
        start_date = self.config.get("start_date")
        if start_date:
            start_date = parse(self.config.get("start_date"))
        rep_key = self.get_starting_timestamp(context)
        return rep_key or start_date
    
    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        if next_page_token:
            params["cursor"] = next_page_token
        if self.replication_key:
            params["sort"] = "asc"
            params["order_by"] = self.replication_key

        start_date = self.get_starting_time(context)
        if self.replication_key and start_date:
            start_date = start_date.isoformat()
            params["begin_time"] = start_date    
        # params["types"] = "item"
        
        return params

    def prepare_request_payload(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Optional[dict]:
        """Prepare the data payload for the REST API request.

        By default, no payload will be sent (return None).
        """
        # TODO: Delete this method if no payload is required. (Most REST APIs.)
        return None

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        """Parse the response and return an iterator of result rows."""
        # TODO: Parse response body and return a set of records.
        yield from extract_jsonpath(self.records_jsonpath, input=response.json())

    def post_process(self, row: dict, context: Optional[dict]) -> dict:
        """As needed, append or transform raw data to match expected structure."""
        # TODO: Delete this method if not needed.
        return row
    
    def request_decorator(self, func: Callable) -> Callable:
        """Instantiate a decorator for handling request failures.

        Developers may override this method to provide custom backoff or retry
        handling.

        Args:
            func: Function to decorate.

        Returns:
            A decorated method.
        """
        decorator: Callable = backoff.on_exception(
            backoff.expo,
            (
                RetriableAPIError,
                requests.exceptions.ReadTimeout,
                requests.exceptions.ConnectionError,
                ConnectionResetError
            ),
            max_tries=5,
            factor=2,
        )(func)
        return decorator
        
